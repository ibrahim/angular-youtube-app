import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.css']
})
export class VideoDetailComponent implements OnInit, OnChanges {
  @Input() video: any;
  videoId;
  videoUrl;
  safeVideoUrl;

  constructor(private sanitizer: DomSanitizer) {
      this.sanitizer = sanitizer;
  }


  ngOnInit() {
    this.videoId = this.video.id.videoId;
    this.videoUrl = `https://www.youtube.com/embed/${this.videoId}`;
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('video-detail ngOnChanges()');
    this.videoId = this.video.id.videoId;
    this.videoUrl = `https://www.youtube.com/embed/${this.videoId}`;
  }

  getSafeVideoUrl () {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.videoUrl);
  }

}
