import { AngularYoutubeAppPage } from './app.po';

describe('angular-youtube-app App', () => {
  let page: AngularYoutubeAppPage;

  beforeEach(() => {
    page = new AngularYoutubeAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
